<a name="module_iterator-worker"></a>

## iterator-worker
Iterator Worker

**See**: [default](#module_iterator-worker--module.exports.default)  

* [iterator-worker](#module_iterator-worker)
    * [module.exports](#exp_module_iterator-worker--module.exports) ⏏
        * [new module.exports(worker, callback)](#new_module_iterator-worker--module.exports_new)
        * _instance_
            * [.started](#module_iterator-worker--module.exports+started)
            * [.finished](#module_iterator-worker--module.exports+finished)
            * [.start()](#module_iterator-worker--module.exports+start) ⇒ <code>Boolean</code>
            * [.join()](#module_iterator-worker--module.exports+join) ⇒ <code>Promise</code>
            * [.stop()](#module_iterator-worker--module.exports+stop) ⇒ <code>Promise</code>
        * _static_
            * [.IteratorWorkerError](#module_iterator-worker--module.exports.IteratorWorkerError)
            * [.default](#module_iterator-worker--module.exports.default)
            * [.start()](#module_iterator-worker--module.exports.start)

<a name="exp_module_iterator-worker--module.exports"></a>

### module.exports ⏏
**Kind**: Exported class  
<a name="new_module_iterator-worker--module.exports_new"></a>

#### new module.exports(worker, callback)
Create an instance of IteratorWorker for a given iterator.
The callback's signature is the same as general node-style callbacks:
		function callback(error, result){}
In the case of an error the callback will be called with only one argument, and the worker will exit.
Otherwise, each time the iterator yields, the callback will be called with the yeilded value
as the second argument, and `undefined` as the first.


| Param | Type | Description |
| --- | --- | --- |
| worker | <code>Iterable</code> \| <code>AsyncIterable</code> | An iterable object |
| callback | <code>function</code> | A function called with each iteration result or error |

<a name="module_iterator-worker--module.exports+started"></a>

#### module.exports.started
True if the worker has started

**Kind**: instance property of [<code>module.exports</code>](#exp_module_iterator-worker--module.exports)  
<a name="module_iterator-worker--module.exports+finished"></a>

#### module.exports.finished
True if the worker has finished

**Kind**: instance property of [<code>module.exports</code>](#exp_module_iterator-worker--module.exports)  
<a name="module_iterator-worker--module.exports+start"></a>

#### module.exports.start() ⇒ <code>Boolean</code>
Starts the iterator in the same event loop.

**Kind**: instance method of [<code>module.exports</code>](#exp_module_iterator-worker--module.exports)  
**Returns**: <code>Boolean</code> - False if the iterator had been started previously, otherwise True  
**Throws**:

- <code>IteratorWorkerError</code> If the worker has already finished.

<a name="module_iterator-worker--module.exports+join"></a>

#### module.exports.join() ⇒ <code>Promise</code>
Wait for your worker to finish naturally.
If your worker uses an infinite loop, this will never resolve;
instead you should call `stop`

**Kind**: instance method of [<code>module.exports</code>](#exp_module_iterator-worker--module.exports)  
**Returns**: <code>Promise</code> - Promise that resolves when the worker stops  
<a name="module_iterator-worker--module.exports+stop"></a>

#### module.exports.stop() ⇒ <code>Promise</code>
Stop the worker after the current iteration.
and wait for it to finish.

**Kind**: instance method of [<code>module.exports</code>](#exp_module_iterator-worker--module.exports)  
**Returns**: <code>Promise</code> - Promise that resolves when the worker stops  
<a name="module_iterator-worker--module.exports.IteratorWorkerError"></a>

#### module.exports.IteratorWorkerError
An error that occurs as part of the management of an IteratorWorker

**Kind**: static class of [<code>module.exports</code>](#exp_module_iterator-worker--module.exports)  
<a name="module_iterator-worker--module.exports.default"></a>

#### module.exports.default
A class representing an (async or sync) iterator,
with methods for starting or stopping its execution.

The iterator will be run in the same event loop,
meaning synchronous code in the worker can still
block the execution of your program.

It is advised you only use this library in low-risk
environments such as development environments and
user scripts. Otherwise you should be running
your worker in a separate thread.

**Kind**: static property of [<code>module.exports</code>](#exp_module_iterator-worker--module.exports)  
<a name="module_iterator-worker--module.exports.start"></a>

#### module.exports.start()
Constructs and starts the iterator worker.
Has the same signature as the regular constructor,
and includes the behaviour of the `start` method.

**Kind**: static method of [<code>module.exports</code>](#exp_module_iterator-worker--module.exports)  
