# IteratorWorker

A class for running an (async or sync) iterator on the current event loop, with methods for starting and stopping its execution.

## Beware

The iterator will be run in the same event loop as the calling code.
This means your worker can affect the performance of the rest of your code,
including causing it to **block your program's execution completely**.

It is advised you only use this library in situations that do not have
high performance requirements, such as development environments
and user scripts. Otherwise you should be running
your worker in a separate thread.

## Example

```javascript
import IteratorWorker from 'iterator-worker';

// Use a sleep functon to simulate waiting on i/o
const sleep = n => new Promise(resolve=>setTimeout(resolve, n));

// Example worker function
// Counts upwards, one number per second
async function* count(){
	let i = 0;
	while(true) {
		await sleep(1000);
		console.log(++i);
		yield i;
	}
}

// Start the worker
const worker = IteratorWorker.start(count());

// The worker is now running and will count every second.
// Sleep our code to simulate doing work.
await sleep(10000);

// Stop the worker
await worker.stop();
```

You can also use the class in a for-await loop, which takes care of starting and stopping the worker automatically:

```javascript
for await(const {} of new IteratorWorker(count())) {
	// This is equivalent to the previous code
	await sleep(10000);
}
```

The advantage of doing this is that it will also close the iterator
in case your code throws an error.
