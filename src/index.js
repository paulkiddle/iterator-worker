/**
 * Iterator Worker
 * @module iterator-worker
 * @see {@link module:iterator-worker.default}
 */

/**
 * An error that occurs as part of the management of an IteratorWorker
 */
export class IteratorWorkerError extends Error {}


/**
 * A class representing an (async or sync) iterator,
 * with methods for starting or stopping its execution.
 *
 * The iterator will be run in the same event loop,
 * meaning synchronous code in the worker can still
 * block the execution of your program.
 *
 * It is advised you only use this library in low-risk
 * environments such as development environments and
 * user scripts. Otherwise you should be running
 * your worker in a separate thread.
 *
 * @name module:iterator-worker.default
 */
export default class IteratorWorker {
	#worker;
	#started = false;
	#finished = false;
	#callback;
	#abort = false;
	#promise;

	/**
	 * Create an instance of IteratorWorker for a given iterator.
	 * The callback's signature is the same as general node-style callbacks:
	 * 		function callback(error, result){}
	 * In the case of an error the callback will be called with only one argument, and the worker will exit.
	 * Otherwise, each time the iterator yields, the callback will be called with the yeilded value
	 * as the second argument, and `undefined` as the first.
	 *
	 * @param {Iterable|AsyncIterable}	worker	An iterable object
	 * @param {Function}	callback	A function called with each iteration result or error
	 */
	constructor(worker, callback = ()=>{}) {
		const o = Object(worker);
		if(!(Symbol.asyncIterator in o || Symbol.iterator in o)) {
			throw new TypeError(worker + ' is not iterable');
		}
		this.#worker = worker;
		this.#callback = callback;
	}

	/**
	 * Constructs and starts the iterator worker.
	 * Has the same signature as the regular constructor,
	 * and includes the behaviour of the `start` method.
	 */
	static start(...args){
		const worker = new IteratorWorker(...args);
		worker.start();
		return worker;
	}

	/**
	 * Starts the iterator in the same event loop.
	 * @returns {Boolean} False if the iterator had been started previously, otherwise True
	 * @throws {IteratorWorkerError} If the worker has already finished.
	 */
	start(){
		if(this.#finished) {
			throw new IteratorWorkerError('The worker has already been run and has finished.');
		}

		if(this.#started) {
			return false;
		}

		this.#started = true;
		this.#promise = this.#start();

		return true;
	}

	// Actual iteraton code
	async #start(){
		try {
			for await(const result of this.#worker) {
				this.#callback(undefined, result);

				if(this.#abort) {
					break;
				}
			}
		} catch(e) {
			this.#callback(e);
			throw e;
		} finally {
			this.#finished = true;
		}
	}

	/**
	 * Alternative way to start and stop the iterator - use it in a `for await` loop
	 * @yields {IteratorWorker} this
	 *
	 * @example
	 * for await(const worker of new IteratorWorker(worker)) {
	 * 	// Do some stuff in the loop body;
	 * 	// When your code ends or throws the iterator will be stopped automatically.
	 * }
	 */
	async * [Symbol.asyncIterator]() {
		this.start();

		try {
			yield this;
		} finally {
			await this.stop();
		}
	}

	/**
	 * Wait for your worker to finish naturally.
	 * If your worker uses an infinite loop, this will never resolve;
	 * instead you should call `stop`
	 * @returns {Promise} Promise that resolves when the worker stops
	 */
	join(){
		return this.#promise;
	}

	/**
	 * Stop the worker after the current iteration.
	 * and wait for it to finish.
	 * @returns {Promise} Promise that resolves when the worker stops
	 */
	stop(){
		this.#abort = true;
		return this.join();
	}

	/**
	 * True if the worker has started
	 */
	get started(){
		return this.#started;
	}

	/**
	 * True if the worker has finished
	 */
	get finished(){
		return this.#finished;
	}
}
