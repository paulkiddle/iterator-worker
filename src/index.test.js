import IteratorWorker, { IteratorWorkerError } from './index.js';
import { jest } from '@jest/globals';

async function* worker(){
	while(true) {
		yield;
	}
}

function* workerSync(){
	while(true) {
		yield;
	}
}

test('Starts and finishes', async ()=>{
	let w;

	for await(w of IteratorWorker.start(worker())) {
		expect(w.started).toBe(true);
		expect(w.finished).toBe(false);
	}

	expect(w.finished).toBe(true);
});

test('Works with sync iterable', async ()=>{
	let w;

	for await(w of IteratorWorker.start(workerSync())) {
		expect(w.started).toBe(true);
		expect(w.finished).toBe(false);
	}

	expect(w.finished).toBe(true);
});

test('Throws TypeError if worker is not iterable', async ()=>{
	expect(()=>new IteratorWorker({})).toThrow(TypeError);
	expect(()=>new IteratorWorker([1,2,3])).not.toThrowError();
});

test('Cannot start a finished iterator', async ()=>{
	const w = IteratorWorker.start(worker());
	await w.stop();
	expect(()=>w.start()).toThrow(IteratorWorkerError);
});

test('Handles exceptions in worker', async() => {
	const e = new Error('Error');
	function* eWorker(){
		yield 1;
		throw e;
	}

	const cb = jest.fn();

	const w = IteratorWorker.start(eWorker(), cb);

	await expect(w.join()).rejects.toBe(e);

	expect(cb).toHaveBeenCalledWith(undefined, 1);
	expect(cb).toHaveBeenCalledWith(e);
});
